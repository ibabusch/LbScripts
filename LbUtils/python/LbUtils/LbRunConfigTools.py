'''

Tools to manage the lb-run configuration


Created on March 3, 2016
@author: Ben Couturier
'''
import json
import logging
import os
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom


def loadConfig(dir = None):
    '''
    Loads the project configuration from the JSON file
    '''
    fname = 'projectConfig.json'
    if dir != None:
        fname = os.path.join(dir, fname)

    res = None
    with open(fname) as f:
        res = json.load(f)

    return res

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    indent(elem)
    return '<?xml version="1.0" encoding="UTF-8"?>\n' \
        + ET.tostring(elem, encoding='utf-8')


class ManifestGenerator( object ):
    '''
    Generates a Manifest.xml file like:

    <?xml version="1.0" encoding="UTF-8"?>
    <manifest>
      <project name="LbScripts" version="HEAD" />
      <heptools>
        <version>83</version>
        <packages>
          <package name="Python" version="2.7.9.p1" />
          <package name="pytools" version="1.9_python2.7" />
        </packages>
       </heptools>
       <used_projects>
         <project name="LHCbGrid" version="v9r3" />
         <project name="Dirac" version="v6r14p24" />
       </used_projects>
    </manifest>

    Given the JSON project configuration.

    '''
    def __init__(self, config):
        self._config = config


    def getDocument(self):
        ''' Builds the XML document based on the config provided '''
        manifest = ET.Element('manifest')
        project = ET.SubElement(manifest, 'project')
        project.set('name', self._config['name'])
        project.set('version', self._config['version'])
        heptoolsConfig = self._config.get('heptools', None)
        if heptoolsConfig != None:
            heptools = ET.SubElement(manifest, 'heptools')

            # Checking the heptools version and setting it
            if heptoolsConfig.get('version') == None:
                raise Exception("Missing version for heptools")
            heptoolsVersion = ET.SubElement(heptools, 'version')
            heptoolsVersion.text = str(heptoolsConfig.get('version'))

            # Checking the heptools binary_tag and setting it
            if heptoolsConfig.get( 'binary_tag' ) == None:
                raise Exception( "Missing binary_tag for heptools" )
            heptoolsBinary = ET.SubElement( heptools, 'binary_tag' )
            heptoolsBinary.text = str( self._config['cmtconfig'] )

            # Now the list of packages
            packagesConfig = heptoolsConfig.get('packages')
            if packagesConfig != None:
                packages =  ET.SubElement(heptools, 'packages')
                for (p, v, t) in packagesConfig:
                    ET.SubElement(packages, 'package', attrib={ "name":p, "version":v})

        exttoolsConfig = self._config.get( 'exttools', None )
        if exttoolsConfig != None:
            exttools = ET.SubElement( manifest, 'exttools' )

            if exttoolsConfig.get( 'binary_tag' ) != None:
              exttoolsVersion = ET.SubElement( exttools, 'binary_tag' )
              exttoolsVersion.text = str( exttoolsConfig.get( 'binary_tag' ) )

            # Now the list of packages
            packagesConfig = exttoolsConfig.get( 'packages' )
            if packagesConfig != None:
                packages = ET.SubElement( exttools, 'packages' )
                for ( p, v, t ) in packagesConfig:
                    ET.SubElement( packages, 'package', attrib = { "name":p, "version":v} )

        usedProjectConfig = self._config.get( 'used_projects', None )
        if usedProjectConfig != None:
            usedProject = ET.SubElement( manifest, 'used_projects' )
            projectConfig = usedProjectConfig.get( 'project' )
            if projectConfig != None:
            # Now the list of packages
               for ( p, v, t ) in projectConfig:
                   ET.SubElement( usedProject, 'project', attrib = { "name":p, "version":v} )

        return manifest


class XEnvGenerator(object):
    '''
    Generates an xenv file like:

<?xml version="1.0" encoding="UTF-8"?>
<env:config xmlns:env="EnvSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="EnvSchema EnvSchema.xsd ">
  <env:default variable="LCG_releases_base">/afs/cern.ch/sw/lcg/releases</env:default>
  <env:default variable="CMTCONFIG">x86_64-slc6-gcc49-opt</env:default>
  <env:default variable="BINARY_TAG">${CMTCONFIG}</env:default>

  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/bin</env:prepend>
  <env:prepend variable="PYTHONPATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/lib/python2.7/site-packages</env:prepend>
  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/Python/2.7.9.p1/${BINARY_TAG}/bin</env:prepend>

  <env:prepend variable="PATH">${.}/LbConfiguration/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbConfiguration/python</env:prepend>
  <env:set variable="LBCONFIGURATIONROOT">${.}/LbConfiguration</env:set>

  <env:prepend variable="PATH">${.}/LbLegacy/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbLegacy/python</env:prepend>
  <env:set variable="LBLEGACYROOT">${.}/LbLegacy</env:set>

  <env:prepend variable="PATH">${.}/LbNightlyTools/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbNightlyTools/python</env:prepend>
  <env:set variable="LBNIGHTLYTOOLSROOT">${.}/LbNightlyTools</env:set>

  <env:prepend variable="PATH">${.}/LbRelease/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbRelease/python</env:prepend>
  <env:set variable="LBRELEASEROOT">${.}/LbRelease</env:set>

  <env:prepend variable="PATH">${.}/LbScriptsPolicy/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbScriptsPolicy/python</env:prepend>
  <env:set variable="LBSCRIPTSPOLICYROOT">${.}/LbScriptsPolicy</env:set>

  <env:prepend variable="PATH">${.}/LbUtils/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbUtils/python</env:prepend>
  <env:set variable="LBUTILSROOT">${.}/LbUtils</env:set>

  <env:prepend variable="CMAKE_PREFIX_PATH">${.}/LbRelease/data/DataPkgEnvs:${.}/LbUtils/cmake</env:prepend>

</env:config>

'''
    def __init__(self, config):
        self._config = config


    def getDocument( self ):
        ''' Builds the XML document based on the config provided '''
        ET.register_namespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        ET.register_namespace('env', 'EnvSchema')
        config = ET.Element('{EnvSchema}config')
        config.set("{http://www.w3.org/2001/XMLSchema-instance}schemaLocation", "EnvSchema EnvSchema.xsd ")

        if self._config.has_key( 'dir_base' ):
          dir_base = self._config['dir_base']
        else:
          dir_base = '.'

        # Adding vars
        def add(tag, var, val):
            d = ET.SubElement(config, tag, attrib={  "variable": var })
            d.text = val
            return d

        def addInclude( tag, val ):
            d = ET.SubElement( config, tag )
            d.text = val
            return d

        add( "env:default", "LCG_releases_base", "/afs/cern.ch/sw/lcg/releases" )
        add( "env:default", "LCG_external_area", "/afs/cern.ch/sw/lcg/external" )
        if self._config.has_key( 'cmtconfig' ):
          add( "env:default", "CMTCONFIG", "%s" % self._config['cmtconfig'] )
        else:
          add( "env:default", "CMTCONFIG", "x86_64-slc6-gcc49-opt" )

        if self._config.has_key( 'python_version' ):
          add( "env:default", "PYTHON_VERSION_TWO", "%s" % self._config['python_version'] )
        else:
          add( "env:default", "PYTHON_VERSION_TWO", "2.7" )

        add( "env:default", "BINARY_TAG", "${CMTCONFIG}" )
        add( "env:prepend", "PYTHONPATH", "%s/%s/%s_%s/InstallArea/${BINARY_TAG}/lib/python${PYTHON_VERSION_TWO}/site-packages" % ( dir_base, self._config['name'].upper(), self._config['name'].upper(), self._config['version'] ) )
        add( "env:prepend", "LD_LIBRARY_PATH", "%s/%s/%s_%s/InstallArea/${BINARY_TAG}/lib" % ( dir_base, self._config['name'].upper(), self._config['name'].upper(), self._config['version'] ) )
        add( "env:prepend", "PATH", "%s/%s/%s_%s/InstallArea/${BINARY_TAG}/bin" % ( dir_base, self._config['name'].upper(), self._config['name'].upper(), self._config['version'] ) )


        projectConfig = self._config.get( 'used_projects', None )
        def addProject( projectConfig ):
          if projectConfig == None:
            return
          projectProject = projectConfig.get( 'project' )
          for p, v, t in projectProject:
            addInclude( "env:search_path", "%s/%s/%s_%s" % ( dir_base, p.upper(), p.upper(), v ) )
            addInclude( "env:search_path", "%s/%s/%s_%s/InstallArea/${BINARY_TAG}" % ( dir_base, p.upper(), p.upper(), v ) )
            addInclude( "env:include", "%s.xenv" % p )
            if p.upper() == 'DIRAC':
              add( "env:prepend", "PATH", "%s/%s/%s_%s/scripts" % ( dir_base, p.upper(), p.upper(), v ) )
              add( "env:prepend", "PYTHONPATH", "%s/%s/%s_%s" % ( dir_base, p.upper(), p.upper(), v ) )

        addProject ( projectConfig )

        heptoolsConfig = self._config.get( 'heptools', None )
        def addHeptools(heptoolsConfig):
            if heptoolsConfig == None:
                return
            heptoolsVersion = heptoolsConfig.get('version')
            heptoolsPackages = heptoolsConfig.get('packages')

            # Identify the python version from the packages
            pythonVersion=None
            for p,v,t in heptoolsPackages:
                if p.upper() == "PYTHON":
                    pythonVersion = '.'.join(v.split('.')[:2])

            # Now add entries for the packages
            for p,v, t in heptoolsPackages:
                if "bin" in t:
                    add( "env:prepend", "PATH", "${LCG_releases_base}/LCG_%s/%s/%s/${BINARY_TAG}/bin" % ( heptoolsVersion, p, v ) )
                if "lib" in t:
                    add( "env:prepend", "LD_LIBRARY_PATH", "${LCG_releases_base}/LCG_%s/%s/%s/${BINARY_TAG}/lib64" % ( heptoolsVersion, p, v ) )
                    add( "env:prepend", "LD_LIBRARY_PATH", "${LCG_releases_base}/LCG_%s/%s/%s/${BINARY_TAG}/lib" % ( heptoolsVersion, p, v ) )
                if "python" in t:
                    add( "env:prepend", "PYTHONPATH", "${LCG_releases_base}/LCG_%s/%s/%s/${BINARY_TAG}/lib64/python${PYTHON_VERSION_TWO}/site-packages" % ( heptoolsVersion, p, v ) )
                    add( "env:prepend", "PYTHONPATH", "${LCG_releases_base}/LCG_%s/%s/%s/${BINARY_TAG}/lib/python${PYTHON_VERSION_TWO}/site-packages" % ( heptoolsVersion, p, v ) )

#  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/bin</env:prepend>
#  <env:prepend variable="PYTHONPATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/lib/python2.7/site-packages</env:prepend>
#  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/Python/2.7.9.p1/${BINARY_TAG}/bin</env:prepend>


        addHeptools(heptoolsConfig)

        exttoolsConfig = self._config.get( 'exttools', None )
        def addExttools( exttoolsConfig ):
            if exttoolsConfig == None:
                return
            exttoolsVersion = exttoolsConfig.get( 'version' )
            exttoolsPackages = exttoolsConfig.get( 'packages' )

            # Identify the python version from the packages
            pythonVersion = None
            for p, v, t in exttoolsPackages:
                if p.upper() == "PYTHON":
                    pythonVersion = '.'.join( v.split( '.' )[:2] )

            # Now add entries for the packages
            for p, v, t in exttoolsPackages:
                if "bin" in t:
                    add( "env:prepend", "PATH", "${LCG_external_area}/Grid/%s/%s/${BINARY_TAG}/bin" % ( p, v ) )
                if "lib" in t:
                    add( "env:prepend", "LD_LIBRARY_PATH", "${LCG_external_area}/Grid/%s/%s/${BINARY_TAG}/lib64" % ( p, v ) )
                    add( "env:prepend", "LD_LIBRARY_PATH", "${LCG_external_area}/Grid/%s/%s/${BINARY_TAG}/lib" % ( p, v ) )
                if "python" in t:
                    add( "env:prepend", "PYTHONPATH", "${LCG_external_area}/Grid/%s/%s/${BINARY_TAG}/lib64/python${PYTHON_VERSION_TWO}/site-packages" % ( p, v ) )
                    add( "env:prepend", "PYTHONPATH", "${LCG_external_area}/Grid/%s/%s/${BINARY_TAG}/lib/python${PYTHON_VERSION_TWO}/site-packages" % ( p, v ) )

#  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/bin</env:prepend>
#  <env:prepend variable="PYTHONPATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/lib/python2.7/site-packages</env:prepend>
#  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/Python/2.7.9.p1/${BINARY_TAG}/bin</env:prepend>


        addExttools( exttoolsConfig )


        def addPackage(name):
            add("env:prepend", "PATH", "${.}/%s/scripts" % name)
            add( "env:prepend", "PYTHONPATH", "${.}/%s/python" % name )
            add( "env:set", "%sROOT" % name.upper(), "${.}/%s" % name )

        if self._config['name'] == 'LbScrips':
            addPackage( "LbConfiguration" )
            addPackage( "LbLegacy" )
            addPackage( "LbNightlyTools" )
            addPackage( "LbRelease" )
            addPackage( "LbScriptsPolicy" )
            addPackage( "LbUtils" )

            add( "env:prepend", "CMAKE_PREFIX_PATH", "${.}/LbRelease/data/DataPkgEnvs:${.}/LbUtils/cmake" )

        return config

if __name__ == "__main__":
    pass
