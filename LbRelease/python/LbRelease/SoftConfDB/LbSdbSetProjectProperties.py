#!/usr/bin/env python
"""

Script to set the properties about a project.

"""
import logging
import sys

from LbUtils.Script import Script
from LbRelease.SoftConfDB.SoftConfDB import SoftConfDB


class LbSdbSetProjectProperties(Script):
    """ Update information about a project (gitlab information)"""

    def defineOpts(self):
        """ Script specific options """
        parser = self.parser
        parser.add_option("-d",
                          dest = "debug",
                          action = "store_true",
                          help = "Display debug output")
        parser.add_option("-r",
                          dest = "reset",
                          action = "store_true",
                          default=False,
                          help = "Reset the properties")
        parser.add_option("--https",
                          dest = "https",
                          action = "store",
                          default = None,
                          help = "Set the Gitlab git repo HTTPS URL")
        parser.add_option("--ssh",
                          dest = "ssh",
                          action = "store",
                          default = None,
                          help = "Set the Gitlab git repo SSH URL")
        parser.add_option("--view",
                          dest = "view",
                          action = "store",
                          default = None,
                          help = "Set the Gitlab view URL")
        parser.add_option("--fromVersion",
                          dest = "fromVersion",
                          action = "store",
                          default = None,
                          help = "Set the version from which the project is handled by these properties")

    def main(self):
        """ Main method for bootstrap and parsing the options.
        It invokes the appropriate method and  """
        self.log = logging.getLogger()

        opts = self.options
        args = self.args
        if opts.debug:
            self.log.setLevel(logging.DEBUG)
        else:
            self.log.setLevel(logging.WARNING)

        if opts.reset:
            if len(args) < 1 :
                self.log.error("Not enough arguments: please specify the project name")
                sys.exit(1)
            else :
                project     = args[0].upper()
                self.mConfDB = SoftConfDB()
                self.mConfDB.resetProjectProperties(project)
                sys.exit(0)
                
        if len(args) < 3 :
            self.log.error("Not enough arguments: please specify the project name, gitlab group and gitlab project name")
            sys.exit(1)
        else :
            project     = args[0].upper()
            gitlabGroup = args[1]
            gitlabName  = args[2]

        # Connect to the ConfDB to update the platform
        self.mConfDB = SoftConfDB()

        self.mConfDB.setProjectProperties(project, gitlabGroup, gitlabName,
                                          gitlabHTTPSURL=opts.https,
                                          gitlabSSHURL=opts.ssh,
                                          gitlabViewURL=opts.view,
                                          fromVersion=opts.fromVersion)
        

if __name__=='__main__':
    sUsage = """%prog project gitlabGroup gitlabName
    Sets the project properties
    """
    s = LbSdbSetProjectProperties(usage=sUsage)
    sys.exit(s.run())


