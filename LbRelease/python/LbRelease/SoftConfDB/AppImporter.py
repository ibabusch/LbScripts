#!/usr/bin/env python
"""
A script to add a project to the Software Configuration DB

"""
import logging
import os
import re
import sys
import urllib2

from LbConfiguration.Repository import getRepositories
from LbRelease import rcs
from LbRelease.SvnTools.Project import translateProject, getProjectCmt
from LbUtils.Processes import callCommand
from LbRelease.SoftConfDB.SoftConfDB import SoftConfDB


# SVN Repositories definition
url = str(getRepositories(protocol='anonymous')["lbsvn"])
lbsvn = rcs.connect(url)

gaudiurl = str(getRepositories(protocol='anonymous')["gaudi"])
gaudisvn = rcs.connect(gaudiurl)

diracurl = str(getRepositories(protocol='anonymous')["dirac"])
diracsvn = rcs.connect(diracurl)

def importerTranslateProject(p, v):
    ''' Function needed to prevent LCGCMT to be passed to translateProject
    as that does not work '''
    if p.lower() == "lcgcmt" or p.lower() == "lcg":
        return (p.upper(), v)
    else:
        return translateProject(p, v)


class GitlabProject:
    """ Helper class to manager projects hosted in gitlab """

    def __init__(self, project, version, projProperties):
        ## takes a map like the following as input
        # "GAUDI": {
        #     "gitlabGroup": "gaudi",
        #     "gitlabHTTPSURL": "https://gitlab.cern.ch",
        #     "gitlabName": "Gaudi",
        #     "gitlabSSHURL": "ssh://git@gitlab.cern.ch:7999",
        #     "gitlabViewURL": "https://gitlab.cern.ch",
        #     "project": "GAUDI"
        #  }
        self.project = project
        self.version = version
        for k in projProperties.keys():
            setattr(self, k, projProperties[k])
        self.log = logging.getLogger()

    def getURL(self, file=None):
        """ Returns the URL at which files can be found in gitlab
        e.g. https://gitlab.cern.ch/gaudi/Gaudi/raw/v27r0/CMakeLists.txt
        """
        prefix =  self.gitlabViewURL + "/" \
                 + self.gitlabGroup + "/" + self.gitlabName + "/raw/" \
                 + self.version
        if file != None:
            return prefix + "/" + file
        else:
            return prefix

    def getToolchain(self):
        self.toolchainurl = self.getURL("toolchain.cmake")
        response = urllib2.urlopen(self.toolchainurl)
        data = response.read()
        return data

    def getCMakeLists(self):
        self.cmakelistsurl = self.getURL("CMakeLists.txt")
        response = urllib2.urlopen(self.cmakelistsurl)
        data = response.read()
        return data


    def getDependencies(self):
        """ Returns the list of project dependencies """
        if self.project.upper() == "GAUDI":
            # For GAUDI we take the dependency in the toolchain file
            data = self.getToolchain()
            r = re.compile("\s*set\s*\(\s*heptools_version\s+(\w+)\s*\)", re.MULTILINE)
            m = r.search(data)
            if m != None:
                self.log.debug("Found match for heptools_version: %s" % m.group(1))
                return  [ ("LCG",  m.group(1)) ]
        else:
            # For all other projects use the gaudi_project macro
            data = self.getCMakeLists()
            # e.g.
            # gaudi_project(LHCb v39r2
            #   USE Gaudi v26r4
            #   DATA Gen/DecFiles
            #        Det/SQLDDDB VERSION v7r*
            #        FieldMap
            #        ParamFiles
            #        PRConfig
            #        RawEventFormat
            #        TCK/HltTCK
            #        TCK/L0TCK VERSION v4r*)

            # Group to one line to avoid dealing with carriage returns, then gets the lists of deps
            # Remove all comments
            # This is NOT ERROR PROOF as it ignores the fact that the # could be within ascript
            # but we need a proepr parser to sort that out.
            alllines = [ l.strip().split("#")[0] for l in data.splitlines() ]
            data = ''.join(alllines)
            r = re.compile("\s*gaudi_project\s*\(\s*(\w+)\s+(\w+)\s+USE\s+(.*)\s+DATA")
            m = r.search(data)

            if m != None:
                # Checking that we have the right section in the CMakeLists
                tproj = m.group(1)
                tver =  m.group(2)
                if tproj.upper() != self.project or tver != self.version:
                    self.log.debug("Version in CMakeLists does not match the project")
                    raise Exception("Version mismatch between project and CMakeLists.txt: " + tproj.upper() + "," + tver + " vs " \
                                    + self.project + "," + self.version)

                # Now check the match for the dependencies
                # Splitting the list and grouping into pairs
                tmpdeps =  m.group(3).split()
                deplist = zip(*[tmpdeps[x::2] for x in (0, 1)])
                return deplist

        return []


class AppImporter:
    """ Main scripts class for looking up dependencies.
    It inherits from """

    def __init__(self, autorelease = True):
        # Creating the SoftConfDB Object
        self.mConfDB = SoftConfDB()
        self.log = logging.getLogger()
        self.installArea = None
        self.mAutorelease = autorelease


    def inGitlab(self, project):
        """ Check whether the project is handled in GIT or SVN """
        props = self.mConfDB.getProjectProperties(project.upper())
        if props != None and "gitlabName" in props.keys():
            return True
        else:
            return False

    def gitlabProcessProjectVersion(self, p, v, alreadyDone = [], recreate=False):
        """ Get the dependencies for a single project """
        # Cleanup the project name and version and get the SVN URL
        (proj,ver)=importerTranslateProject(p,v)

        # Getting the project properties and locating the CMakeLists
        props = self.mConfDB.getProjectProperties(proj.upper())
        gp = GitlabProject(proj, ver, props)
        deps =  gp.getDependencies()

        # Formatting the project name/version
        corver = ver
        if proj in ver:
            corver = ver.replace(proj + "_", "")
        proj = proj.upper()

        # Looking for the project version in the DB
        tmp = self.mConfDB.findVersion(proj, ver)
        createNode = False
        node_parent = None

        # First checking if the node is there with the correct revision
        if len(tmp) != 0:
            node = tmp[0][0]
            node_parent = node
            # Need to add commit to the DB
        #If the node does not exist just create it...
        else:
            createNode = True

        if createNode:
            self.log.warning("Creating project %s %s" % (proj, ver))
            node_parent = self.mConfDB.getOrCreatePV(proj, corver)
            # If releasing is needed!
            if self.mAutorelease and proj.upper() not in [ "LCG", "LCGCMT"]:
                self.log.warning("Requesting release of %s %s" % (proj, corver))
                self.mConfDB.setReleaseFlag(proj, corver)

        if len(deps) == 0:
            self.log.warning("No dependencies found for %s %s" % (p, v))
            return node_parent

        # Now creating the dependencies
        for (dp, dv) in deps:
            if dp in dv:
                dv = dv.replace(dp + "_", "")
            dp = dp.upper()
            self.log.warning("Find project %s %s" % (dp, dv))
            node_child = self.processProjectVersion(dp, dv, alreadyDone, recreate)

            # Now checking if the links exist
            if self.mConfDB.nodesHaveRelationship(node_parent, node_child, "REQUIRES"):
                self.log.warning("Pre-existing dependency (%s, %s)-[:REQUIRES]->(%s, %s)" % (proj, ver, dp, dv))
            else:
                self.log.warning("Adding dependency (%s, %s)-[:REQUIRES]->(%s, %s)" % (proj, ver, dp, dv))
                self.mConfDB.addRequires(node_parent, node_child)

        return node_parent



    def processProjectVersion(self, p, v, alreadyDone = [], recreate=False):
        """ Get the dependencies for a single project """
        # Cleanup the project name and version and get the SVN URL
        (proj,ver)=importerTranslateProject(p,v)
        tagpath = ""

        # Now checking whether we should get the infor from SVN of GIT
        gitlab = self.inGitlab(proj)

        if gitlab:
            # In this case use the new code
            # This should be cleanup up but in the transition period
            # we'll use this hack
            self.log.warning("Project %s is in Gitlab" % proj)
            return self.gitlabProcessProjectVersion(p, v)

        # Getting the project.cmt file with dependencies
        if proj.upper() == "GANGA":
            projcmt = self.getGangaProjectCMT(ver)
        else:
            if proj.upper() == "GAUDI":
                tagpath = gaudisvn.url(proj,ver, isProject=True)
            elif proj.upper() == "LHCBDIRAC" or proj.upper() == "DIRAC":
                tagpath = diracsvn.url(proj,ver, isProject=True)
            else:
                tagpath=lbsvn.url(proj,ver, isProject=True)
            self.log.debug("SVN PATH:" + tagpath)
            # Get the project.cmt file and parse it to retrieve the dependencies
            #if not tagpath.endswith("cmt"):
            #    tagpath += "/cmt"
            projcmt=getProjectCmt(tagpath).strip()
            self.log.debug("Project tag SVN path:" + tagpath)
        deps = []

        # Formatting the project name/version
        corver = ver
        if proj in ver:
            corver = ver.replace(proj + "_", "")
        proj = proj.upper()

        # Looking for the project version in the DB
        tmp = self.mConfDB.findVersion(proj, ver)
        rev = getPathLastRev(tagpath)
        createNode = False
        node_parent = None

        # First checking if the node is there with the correct revision
        if len(tmp) != 0:
            node = tmp[0][0]
            node_parent = node
            noderev = node["Rev"]
            if noderev != None:
                self.log.warning("Project %s %s already exists with revision %s (path rev: %s)" % (proj,
                                                                                                   ver,
                                                                                                   noderev,
                                                                                                   rev))
                if rev != noderev and recreate:
                    self.log.warning("Mismatch in revisions for %s %s, recreating" % (proj, ver))
                    self.log.warning("WARNING was a retag of  %s %s done? you may need to call "
                                     " lb-sdb-deletepv <proj> <ver> and reimport if "
                                     " it is the case " % (proj, ver))

                    try:
                        self.mConfDB.deletePV(proj, corver)
                    except Exception, e:
                        self.log.error(e)
                    createNode = True


            else:
                self.log.warning("Project %s %s already exists without revision" % (proj,ver))

        #If the node does not exist just create it...
        else:
            createNode = True

        if createNode:
            self.log.warning("Creating project %s %s revision in SVN: %s" % (proj, ver, rev))
            node_parent = self.mConfDB.getOrCreatePV(proj, corver)
            node_parent["Rev"] = rev
            # If releasing is needed!
            if self.mAutorelease:
                self.log.warning("Requesting release of %s %s" % (proj, corver))
                self.mConfDB.setReleaseFlag(proj, corver)


        self.log.warning("Now checking dependencies for %s %s" % (p, v))
        for l in projcmt.splitlines():
            m = re.match("\s*use\s+(\w+)\s+([\w\*]+)", l)
            if m != None:
                dp = m.group(1)
                dv = m.group(2)
                deps.append((dp, dv))

        if len(deps) == 0:
            self.log.warning("No dependencies found for %s %s" % (p, v))
            return node_parent

        # Now creating the dependencies
        for (dp, dv) in deps:
            if dp in dv:
                dv = dv.replace(dp + "_", "")
            dp = dp.upper()
            self.log.warning("Find project %s %s" % (dp, dv))
            node_child = self.processProjectVersion(dp, dv, alreadyDone, recreate)

            # Now checking if the links exist
            if self.mConfDB.nodesHaveRelationship(node_parent, node_child, "REQUIRES"):
                self.log.warning("Pre-existing dependency (%s, %s)-[:REQUIRES]->(%s, %s)" % (proj, ver, dp, dv))
            else:
                self.log.warning("Adding dependency (%s, %s)-[:REQUIRES]->(%s, %s)" % (proj, ver, dp, dv))
                self.mConfDB.addRequires(node_parent, node_child)

        return node_parent

    def getGangaProjectCMT(self, gangaVersion):
        """ Reads the Project.cmt file for Ganga """
        if self.installArea == None:
            raise Exception("Can only get Ganga dependencies from an install area! - Set it on the AppImporter first...")

        pname = "GANGA"
        f = open(os.path.join(self.installArea, pname, pname + "_" + gangaVersion, "cmt", "project.cmt"), "r")
        txt = f.read()
        f.close()
        return txt


def getProjectLastRev(project, version):
    ''' Get the latest revision of a project in SVN
    Allows to check for retagging '''
    (proj,ver)=importerTranslateProject(project, version)
    tagpath = ""

    # Getting the project.cmt file with dependencies
    if proj.upper() == "GANGA":
        projcmt = self.getGangaProjectCMT(ver)
    else:
        if proj.upper() == "GAUDI":
            tagpath = gaudisvn.url(proj,ver, isProject=True)
        elif proj.upper() == "LHCBDIRAC" or proj.upper() == "DIRAC":
            tagpath = diracsvn.url(proj,ver, isProject=True)
        else:
            tagpath=lbsvn.url(proj,ver, isProject=True)
    return getPathLastRev(tagpath)


def getPathLastRev(path):
    ''' Check the SVN revision of the given SVN path '''
    rev = None
    infostr = callCommand('svn','info',path)[0]
    for l in infostr.splitlines():
        m = re.match("Last Changed Rev: (\d+)", l)
        if m != None:
            rev = m.group(1)
            break
    return rev


